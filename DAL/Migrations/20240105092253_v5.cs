﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    public partial class v5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "media",
                columns: table => new
                {
                    MediaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AbsolutePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AbsoluteUri = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContentLength = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mime = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OriginalFileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RelativePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__media__B2C2B5CF0BFF1C7D", x => x.MediaId);
                });

            migrationBuilder.CreateTable(
                name: "productCategory",
                columns: table => new
                {
                    CategoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryName = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CategoryIcon = table.Column<string>(type: "varchar(300)", unicode: false, maxLength: 300, nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__productC__19093A2B7278C71F", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "ResetPasswordTokens",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Code = table.Column<int>(type: "int", maxLength: 5, nullable: false),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpireTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResetPasswordTokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "Urls",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Path = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Method = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Urls", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vouchers",
                columns: table => new
                {
                    VoucherId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Type = table.Column<bool>(type: "bit", nullable: true),
                    NameVoucher = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    Unit = table.Column<bool>(type: "bit", nullable: true),
                    Value = table.Column<int>(type: "int", nullable: true),
                    MaxValue = table.Column<double>(type: "float", nullable: true),
                    MinValue = table.Column<double>(type: "float", nullable: true),
                    TLimitQuantity = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vouchers", x => x.VoucherId);
                });

            migrationBuilder.CreateTable(
                name: "products",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductName = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Decription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Creat_At = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_At = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_products", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK__products__Catego__19DFD96B",
                        column: x => x.CategoryId,
                        principalTable: "productCategory",
                        principalColumn: "CategoryID");
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    UserID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    PassWord = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Avatar = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    FirsName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Create_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modifie_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    RoleID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.UserID);
                    table.ForeignKey(
                        name: "FK__user__RoleID__0C85DE4D",
                        column: x => x.RoleID,
                        principalTable: "role",
                        principalColumn: "RoleId");
                });

            migrationBuilder.CreateTable(
                name: "RoleUrls",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UrlId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleUrls", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleUrls_role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "role",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleUrls_Urls_UrlId",
                        column: x => x.UrlId,
                        principalTable: "Urls",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "productVariant",
                columns: table => new
                {
                    ProductVariantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductVariantName = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Bar_code = table.Column<string>(type: "char(300)", unicode: false, fixedLength: true, maxLength: 300, nullable: true),
                    Inventory_quantity = table.Column<double>(type: "float", nullable: true),
                    Sku = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Price = table.Column<double>(type: "float", nullable: true),
                    ImportPrice = table.Column<double>(type: "float", nullable: true),
                    Create_At = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_At = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_productVariant", x => x.ProductVariantId);
                    table.ForeignKey(
                        name: "FK_productVariant_products",
                        column: x => x.ProductId,
                        principalTable: "products",
                        principalColumn: "ProductId");
                });

            migrationBuilder.CreateTable(
                name: "cartSession",
                columns: table => new
                {
                    CartSessionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Count = table.Column<int>(type: "int", nullable: true),
                    Total = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    Created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cartSession", x => x.CartSessionId);
                    table.ForeignKey(
                        name: "FK__cartSessi__UserI__22751F6C",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "UserID");
                });

            migrationBuilder.CreateTable(
                name: "user_addres",
                columns: table => new
                {
                    UserAddressID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Province = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    City_District = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Ward_Commune = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    AddressDetail = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    TypeAdress = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    IsDefault = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__user_add__5961BB97BA5D2B0C", x => x.UserAddressID);
                    table.ForeignKey(
                        name: "FK__user_addr__UserI__0D7A0286",
                        column: x => x.UserID,
                        principalTable: "user",
                        principalColumn: "UserID");
                });

            migrationBuilder.CreateTable(
                name: "cartSessionDetail",
                columns: table => new
                {
                    CartSessionDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductVariant_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CartSessionId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    Create_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cartSessionDetail", x => x.CartSessionDetailId);
                    table.ForeignKey(
                        name: "FK__cartSessi__Produ__2B0A656D",
                        column: x => x.ProductVariant_Id,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                });

            migrationBuilder.CreateTable(
                name: "productVarianOptions",
                columns: table => new
                {
                    VarianOptionsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductVarianId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    VarianOptionName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__productV__7B60FA54B67AE66D", x => x.VarianOptionsId);
                    table.ForeignKey(
                        name: "FK__productVa__Produ__2A164134",
                        column: x => x.ProductVarianId,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                });

            migrationBuilder.CreateTable(
                name: "warehous",
                columns: table => new
                {
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WarehouseCode = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ProductVariant_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true),
                    Created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__warehous__2608AFF9B58680D5", x => x.WarehouseId);
                    table.ForeignKey(
                        name: "FK__warehous__Produc__2BFE89A6",
                        column: x => x.ProductVariant_Id,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                });

            migrationBuilder.CreateTable(
                name: "Oder",
                columns: table => new
                {
                    OderItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CartSessionId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    Total = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Create_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Oder__0B7EBA0F98D3700B", x => x.OderItemId);
                    table.ForeignKey(
                        name: "FK__Oder__OderItemId__7E37BEF6",
                        column: x => x.OderItemId,
                        principalTable: "cartSession",
                        principalColumn: "CartSessionId");
                });

            migrationBuilder.CreateTable(
                name: "productVariantOptionValue",
                columns: table => new
                {
                    VariantOptionValueID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VariantOptionsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    VariantOptionsValueName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__productV__6582C358F020A137", x => x.VariantOptionValueID);
                    table.ForeignKey(
                        name: "FK__productVa__Varia__17F790F9",
                        column: x => x.VariantOptionsId,
                        principalTable: "productVarianOptions",
                        principalColumn: "VarianOptionsId");
                });

            migrationBuilder.CreateTable(
                name: "deliveryBill",
                columns: table => new
                {
                    DeliveryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    EmployeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__delivery__626D8FCEF5B74C8D", x => x.DeliveryId);
                    table.ForeignKey(
                        name: "FK__deliveryB__Emplo__08B54D69",
                        column: x => x.EmployeeId,
                        principalTable: "user",
                        principalColumn: "UserID");
                    table.ForeignKey(
                        name: "FK__deliveryB__Wareh__02FC7413",
                        column: x => x.WarehouseId,
                        principalTable: "warehous",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "recivedBill",
                columns: table => new
                {
                    RecivedBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    EmployeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_recivedBill", x => x.RecivedBillId);
                    table.ForeignKey(
                        name: "FK__recivedBi__Emplo__09A971A2",
                        column: x => x.EmployeeId,
                        principalTable: "user",
                        principalColumn: "UserID");
                    table.ForeignKey(
                        name: "FK__recivedBi__Wareh__2FCF1A8A",
                        column: x => x.WarehouseId,
                        principalTable: "warehous",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "ReturnAcceptBill",
                columns: table => new
                {
                    ReturnAcceptBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReturnAcceptBillName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnAcceptBill", x => x.ReturnAcceptBillId);
                    table.ForeignKey(
                        name: "FK_ReturnAcceptBill_warehous",
                        column: x => x.WarehouseId,
                        principalTable: "warehous",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "ReturnBill",
                columns: table => new
                {
                    ReturnBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnBill", x => x.ReturnBillId);
                    table.ForeignKey(
                        name: "FK_ReturnBill_warehous",
                        column: x => x.WarehouseId,
                        principalTable: "warehous",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "WarehouseImportBill",
                columns: table => new
                {
                    WarehouseImportBill = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WarehouseImportBill", x => x.WarehouseImportBill);
                    table.ForeignKey(
                        name: "FK_WarehouseImportBill_warehous",
                        column: x => x.WarehouseId,
                        principalTable: "warehous",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "OderDetail",
                columns: table => new
                {
                    OderDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductVariantId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OderItemID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    PaymentId = table.Column<int>(type: "int", nullable: true),
                    Create_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OderDetail", x => x.OderDetailId);
                    table.ForeignKey(
                        name: "FK__OderDetai__OderD__75A278F5",
                        column: x => x.OderDetailId,
                        principalTable: "Oder",
                        principalColumn: "OderItemId");
                    table.ForeignKey(
                        name: "FK__OderDetai__Produ__29221CFB",
                        column: x => x.ProductVariantId,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                });

            migrationBuilder.CreateTable(
                name: "paymentDetail",
                columns: table => new
                {
                    PaymentDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OderItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Total = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    Provider = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    status = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Create_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_paymentDetail", x => x.PaymentDetailId);
                    table.ForeignKey(
                        name: "FK__paymentDe__OderI__114A936A",
                        column: x => x.OderItemId,
                        principalTable: "Oder",
                        principalColumn: "OderItemId");
                });

            migrationBuilder.CreateTable(
                name: "deliveryBillDetails",
                columns: table => new
                {
                    DeliveryBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductVariant_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DeliveryId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    WarehouseId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    NumberofRequest = table.Column<int>(type: "int", nullable: true),
                    ExportQuantity = table.Column<int>(type: "int", nullable: true),
                    Created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Modified_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__delivery__A24F9EFA04CCF65F", x => x.DeliveryBillId);
                    table.ForeignKey(
                        name: "FK__deliveryB__Deliv__2EDAF651",
                        column: x => x.DeliveryId,
                        principalTable: "deliveryBill",
                        principalColumn: "DeliveryId");
                    table.ForeignKey(
                        name: "FK__deliveryB__Produ__31B762FC",
                        column: x => x.ProductVariant_Id,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                    table.ForeignKey(
                        name: "FK__deliveryB__Wareh__02084FDA",
                        column: x => x.WarehouseId,
                        principalTable: "warehous",
                        principalColumn: "WarehouseId");
                });

            migrationBuilder.CreateTable(
                name: "RecivedBillDetail",
                columns: table => new
                {
                    RecivedBillDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RecivedBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductVariantId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductVariantIName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    NumberofRequest = table.Column<int>(type: "int", nullable: true),
                    ExportQuantity = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecivedBillDetail", x => x.RecivedBillDetailId);
                    table.ForeignKey(
                        name: "FK__RecivedBi__Produ__367C1819",
                        column: x => x.ProductVariantId,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                    table.ForeignKey(
                        name: "FK__RecivedBi__Reciv__5AEE82B9",
                        column: x => x.RecivedBillId,
                        principalTable: "recivedBill",
                        principalColumn: "RecivedBillId");
                });

            migrationBuilder.CreateTable(
                name: "ReturnAccpetBillDetail",
                columns: table => new
                {
                    ReturnAcceptBillDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReturnAcceptBillID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductVariantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NumberOfRequest = table.Column<int>(type: "int", nullable: true),
                    ExportQuantity = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnAccpetBillDetail", x => x.ReturnAcceptBillDetailId);
                    table.ForeignKey(
                        name: "FK_ReturnAccpetBillDetail_productVariant",
                        column: x => x.ProductVariantId,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                    table.ForeignKey(
                        name: "FK_ReturnAccpetBillDetail_ReturnAcceptBill",
                        column: x => x.ReturnAcceptBillID,
                        principalTable: "ReturnAcceptBill",
                        principalColumn: "ReturnAcceptBillId");
                });

            migrationBuilder.CreateTable(
                name: "ReturnBillDetail",
                columns: table => new
                {
                    ReturnBillDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReturnBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductVariantId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    NumberofRequest = table.Column<int>(type: "int", nullable: true),
                    ExportQuantity = table.Column<int>(type: "int", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnBillDetail", x => x.ReturnBillDetailId);
                    table.ForeignKey(
                        name: "FK_ReturnBillDetail_productVariant",
                        column: x => x.ProductVariantId,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                    table.ForeignKey(
                        name: "FK_ReturnBillDetail_ReturnBill",
                        column: x => x.ReturnBillId,
                        principalTable: "ReturnBill",
                        principalColumn: "ReturnBillId");
                });

            migrationBuilder.CreateTable(
                name: "WarehouseImportDetail",
                columns: table => new
                {
                    WarehouseImportBillDetailId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WarehouseImportBillId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductVariantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InportQuantity = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WarehouseImportDetail", x => x.WarehouseImportBillDetailId);
                    table.ForeignKey(
                        name: "FK_WarehouseImportDetail_productVariant",
                        column: x => x.ProductVariantId,
                        principalTable: "productVariant",
                        principalColumn: "ProductVariantId");
                    table.ForeignKey(
                        name: "FK_WarehouseImportDetail_WarehouseImportBill",
                        column: x => x.WarehouseImportBillId,
                        principalTable: "WarehouseImportBill",
                        principalColumn: "WarehouseImportBill");
                });

            migrationBuilder.InsertData(
                table: "role",
                columns: new[] { "RoleId", "IsActive", "RoleName" },
                values: new object[] { new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"), true, "Admin" });

            migrationBuilder.InsertData(
                table: "user",
                columns: new[] { "UserID", "Avatar", "Create_at", "Email", "FirsName", "LastName", "Modifie_at", "PassWord", "PhoneNumber", "RoleID", "Status", "UserName" },
                values: new object[] { new Guid("921dad0f-65cd-4dca-b274-ca198b556409"), null, new DateTime(2024, 1, 5, 16, 22, 52, 877, DateTimeKind.Local).AddTicks(4009), "admin@test.tt", "Admin", "Admin", null, "Test@123", "0123456789", new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"), 0, "administrator" });

            migrationBuilder.CreateIndex(
                name: "IX_cartSession_UserId",
                table: "cartSession",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_cartSessionDetail_ProductVariant_Id",
                table: "cartSessionDetail",
                column: "ProductVariant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryBill_EmployeeId",
                table: "deliveryBill",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryBill_WarehouseId",
                table: "deliveryBill",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryBillDetails_DeliveryId",
                table: "deliveryBillDetails",
                column: "DeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryBillDetails_ProductVariant_Id",
                table: "deliveryBillDetails",
                column: "ProductVariant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryBillDetails_WarehouseId",
                table: "deliveryBillDetails",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_OderDetail_ProductVariantId",
                table: "OderDetail",
                column: "ProductVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_paymentDetail_OderItemId",
                table: "paymentDetail",
                column: "OderItemId");

            migrationBuilder.CreateIndex(
                name: "IX_products_CategoryId",
                table: "products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_productVarianOptions_ProductVarianId",
                table: "productVarianOptions",
                column: "ProductVarianId");

            migrationBuilder.CreateIndex(
                name: "IX_productVariant_ProductId",
                table: "productVariant",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_productVariantOptionValue_VariantOptionsId",
                table: "productVariantOptionValue",
                column: "VariantOptionsId");

            migrationBuilder.CreateIndex(
                name: "IX_recivedBill_EmployeeId",
                table: "recivedBill",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_recivedBill_WarehouseId",
                table: "recivedBill",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_RecivedBillDetail_ProductVariantId",
                table: "RecivedBillDetail",
                column: "ProductVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_RecivedBillDetail_RecivedBillId",
                table: "RecivedBillDetail",
                column: "RecivedBillId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnAcceptBill_WarehouseId",
                table: "ReturnAcceptBill",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnAccpetBillDetail_ProductVariantId",
                table: "ReturnAccpetBillDetail",
                column: "ProductVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnAccpetBillDetail_ReturnAcceptBillID",
                table: "ReturnAccpetBillDetail",
                column: "ReturnAcceptBillID");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnBill_WarehouseId",
                table: "ReturnBill",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnBillDetail_ProductVariantId",
                table: "ReturnBillDetail",
                column: "ProductVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnBillDetail_ReturnBillId",
                table: "ReturnBillDetail",
                column: "ReturnBillId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleUrls_RoleId_UrlId",
                table: "RoleUrls",
                columns: new[] { "RoleId", "UrlId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleUrls_UrlId",
                table: "RoleUrls",
                column: "UrlId");

            migrationBuilder.CreateIndex(
                name: "IX_Urls_Path_Method",
                table: "Urls",
                columns: new[] { "Path", "Method" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_RoleID",
                table: "user",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_user_addres_UserID",
                table: "user_addres",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_warehous_ProductVariant_Id",
                table: "warehous",
                column: "ProductVariant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_WarehouseImportBill_WarehouseId",
                table: "WarehouseImportBill",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WarehouseImportDetail_ProductVariantId",
                table: "WarehouseImportDetail",
                column: "ProductVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_WarehouseImportDetail_WarehouseImportBillId",
                table: "WarehouseImportDetail",
                column: "WarehouseImportBillId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "cartSessionDetail");

            migrationBuilder.DropTable(
                name: "deliveryBillDetails");

            migrationBuilder.DropTable(
                name: "media");

            migrationBuilder.DropTable(
                name: "OderDetail");

            migrationBuilder.DropTable(
                name: "paymentDetail");

            migrationBuilder.DropTable(
                name: "productVariantOptionValue");

            migrationBuilder.DropTable(
                name: "RecivedBillDetail");

            migrationBuilder.DropTable(
                name: "ResetPasswordTokens");

            migrationBuilder.DropTable(
                name: "ReturnAccpetBillDetail");

            migrationBuilder.DropTable(
                name: "ReturnBillDetail");

            migrationBuilder.DropTable(
                name: "RoleUrls");

            migrationBuilder.DropTable(
                name: "user_addres");

            migrationBuilder.DropTable(
                name: "Vouchers");

            migrationBuilder.DropTable(
                name: "WarehouseImportDetail");

            migrationBuilder.DropTable(
                name: "deliveryBill");

            migrationBuilder.DropTable(
                name: "Oder");

            migrationBuilder.DropTable(
                name: "productVarianOptions");

            migrationBuilder.DropTable(
                name: "recivedBill");

            migrationBuilder.DropTable(
                name: "ReturnAcceptBill");

            migrationBuilder.DropTable(
                name: "ReturnBill");

            migrationBuilder.DropTable(
                name: "Urls");

            migrationBuilder.DropTable(
                name: "WarehouseImportBill");

            migrationBuilder.DropTable(
                name: "cartSession");

            migrationBuilder.DropTable(
                name: "warehous");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "productVariant");

            migrationBuilder.DropTable(
                name: "role");

            migrationBuilder.DropTable(
                name: "products");

            migrationBuilder.DropTable(
                name: "productCategory");
        }
    }
}
