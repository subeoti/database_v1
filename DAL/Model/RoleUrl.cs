﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class RoleUrl
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public Guid UrlId { get; set; }
        public Role Role { get;  set; }
        public Url Url { get;  set; }
    }
}
