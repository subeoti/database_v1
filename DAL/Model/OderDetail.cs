﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class OderDetail
    {
        public Guid OderDetailId { get; set; }
        public Guid? ProductVariantId { get; set; }
        public Guid? OderItemId { get; set; }
        public decimal? Price { get; set; }
        public int? PaymentId { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual Oder OderDetailNavigation { get; set; }
        public virtual ProductVariant ProductVariant { get; set; }
    }
}
