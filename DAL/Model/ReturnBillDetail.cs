﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ReturnBillDetail
    {
        public Guid ReturnBillDetailId { get; set; }
        public Guid? ReturnBillId { get; set; }
        public Guid? ProductVariantId { get; set; }
        public int? NumberofRequest { get; set; }
        public int? ExportQuantity { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }

        public virtual ReturnBill ReturnBill { get; set; }
        public virtual ProductVariant ReturnBillDetailNavigation { get; set; }
    }
}
