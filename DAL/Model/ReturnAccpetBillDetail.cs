﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ReturnAccpetBillDetail
    {
        public Guid ReturnAcceptBillDetailId { get; set; }
        public Guid ReturnAcceptBillId { get; set; }
        public Guid ProductVariantId { get; set; }
        public int? NumberOfRequest { get; set; }
        public int ExportQuantity { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual ReturnAcceptBill ReturnAcceptBill { get; set; }
    }
}
