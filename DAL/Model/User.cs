﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class User
    {
        public User()
        {
            CartSessions = new HashSet<CartSession>();
            DeliveryBills = new HashSet<DeliveryBill>();
            RecivedBills = new HashSet<RecivedBill>();
            UserAddres = new HashSet<UserAddre>();
        }

        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifieAt { get; set; }
        public Guid? RoleId { get; set; }
        public int Status { get; set; }
        public virtual Role Role { get; set; }
        public virtual ICollection<CartSession> CartSessions { get; set; }
        public virtual ICollection<DeliveryBill> DeliveryBills { get; set; }
        public virtual ICollection<RecivedBill> RecivedBills { get; set; }
        public virtual ICollection<UserAddre> UserAddres { get; set; }
    }
}
