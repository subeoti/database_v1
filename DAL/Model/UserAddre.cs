﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class UserAddre
    {
        public Guid UserAddressId { get; set; }
        public Guid? UserId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Province { get; set; }
        public string CityDistrict { get; set; }
        public string WardCommune { get; set; }
        public string AddressDetail { get; set; }
        public string TypeAdress { get; set; }
        public bool? IsDefault { get; set; }

        public virtual User User { get; set; }
    }
}
