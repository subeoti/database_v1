﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class CartSessionDetail
    {
        public Guid CartSessionDetailId { get; set; }
        public Guid? ProductVariantId { get; set; }
        public Guid? CartSessionId { get; set; }
        public Guid? UserId { get; set; }
        public decimal? Price { get; set; }
        public int? Quantity { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
    }
}
