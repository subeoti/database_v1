﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ProductVariantOptionValue
    {
        public Guid VariantOptionValueId { get; set; }
        public Guid? VariantOptionsId { get; set; }
        public string VariantOptionsValueName { get; set; }
        public bool? Status { get; set; }

        public virtual ProductVarianOption VariantOptions { get; set; }
    }
}
