﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ReturnAcceptBill
    {
        public ReturnAcceptBill()
        {
            ReturnAccpetBillDetails = new HashSet<ReturnAccpetBillDetail>();
        }

        public Guid ReturnAcceptBillId { get; set; }
        public string ReturnAcceptBillName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public Guid? WarehouseId { get; set; }
        public int? Status { get; set; }

        public virtual Warehou Warehouse { get; set; }
        public virtual ICollection<ReturnAccpetBillDetail> ReturnAccpetBillDetails { get; set; }
    }
}
