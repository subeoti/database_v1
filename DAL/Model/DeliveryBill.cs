﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class DeliveryBill
    {
        public DeliveryBill()
        {
            DeliveryBillDetails = new HashSet<DeliveryBillDetail>();
        }

        public Guid DeliveryId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public Guid? EmployeeId { get; set; }
        public Guid? WarehouseId { get; set; }
        public int? Status { get; set; }

        public virtual User Employee { get; set; }
        public virtual Warehou Warehouse { get; set; }
        public virtual ICollection<DeliveryBillDetail> DeliveryBillDetails { get; set; }
    }
}
