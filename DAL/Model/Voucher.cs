﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Voucher
    {
        public Guid VoucherId { get; set; }
        public bool? Type { get; set; } // loại mã giảm giá (Tự động hay nhập tay)
        public string? NameVoucher { get; set; } //tên voucher
        public int? Quantity { get; set; } //Số lần sử dụng
        public bool? Unit { get; set; } //Đơn vị
        public int? Value { get; set; }
        public double? MaxValue { get; set; }
        public double? MinValue { get; set; }
        public int? TLimitQuantity { get; set; } // Phân loại voucher sử dụng cho những TK nào
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? Status { get; set; }
    }
}
