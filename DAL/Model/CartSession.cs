﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class CartSession
    {
        public Guid CartSessionId { get; set; }
        public Guid? UserId { get; set; }
        public int? Count { get; set; }
        public decimal? Total { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual User User { get; set; }
        public virtual Oder Oder { get; set; }
    }
}
