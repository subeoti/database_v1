﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ProductVariant
    {
        public ProductVariant()
        {
            CartSessionDetails = new HashSet<CartSessionDetail>();
            DeliveryBillDetails = new HashSet<DeliveryBillDetail>();
            OderDetails = new HashSet<OderDetail>();
            ProductVarianOptions = new HashSet<ProductVarianOption>();
            RecivedBillDetails = new HashSet<RecivedBillDetail>();
            ReturnAccpetBillDetails = new HashSet<ReturnAccpetBillDetail>();
            Warehous = new HashSet<Warehou>();
            WarehouseImportDetails = new HashSet<WarehouseImportDetail>();
            ReturnBillDetails  = new HashSet<ReturnBillDetail>();
        }

        public Guid ProductVariantId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid? CategoryId { get; set; }
        public string ProductVariantName { get; set; }
        public string BarCode { get; set; }
        public double? InventoryQuantity { get; set; }
        public string Sku { get; set; }
        public double? Price { get; set; }
        public double? ImportPrice { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public bool? Status { get; set; }

        public virtual Product Product { get; set; }
        public virtual ICollection<ReturnBillDetail> ReturnBillDetails { get; set; }
        public virtual ICollection<CartSessionDetail> CartSessionDetails { get; set; }
        public virtual ICollection<DeliveryBillDetail> DeliveryBillDetails { get; set; }
        public virtual ICollection<OderDetail> OderDetails { get; set; }
        public virtual ICollection<ProductVarianOption> ProductVarianOptions { get; set; }
        public virtual ICollection<RecivedBillDetail> RecivedBillDetails { get; set; }
        public virtual ICollection<ReturnAccpetBillDetail> ReturnAccpetBillDetails { get; set; }
        public virtual ICollection<Warehou> Warehous { get; set; }
        public virtual ICollection<WarehouseImportDetail> WarehouseImportDetails { get; set; }
    }
}
