﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductVariants = new HashSet<ProductVariant>();
        }

        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string Decription { get; set; }
        public Guid? CategoryId { get; set; }
        public DateTime? CreatAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public bool? Status { get; set; }

        public virtual ProductCategory Category { get; set; }
        public virtual ICollection<ProductVariant> ProductVariants { get; set; }
    }
}
