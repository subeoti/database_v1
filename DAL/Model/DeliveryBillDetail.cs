﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class DeliveryBillDetail
    {
        public Guid DeliveryBillId { get; set; }
        public Guid? ProductVariantId { get; set; }
        public Guid? DeliveryId { get; set; }
        public Guid? WarehouseId { get; set; }
        public int? NumberofRequest { get; set; }
        public int? ExportQuantity { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public int? Status { get; set; }

        public virtual DeliveryBill Delivery { get; set; }
        public virtual ProductVariant ProductVariant { get; set; }
        public virtual Warehou Warehouse { get; set; }
    }
}
