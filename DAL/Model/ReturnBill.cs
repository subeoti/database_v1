﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ReturnBill
    {
        public ReturnBill()
        {
            ReturnBillDetails = new HashSet<ReturnBillDetail>();
        }

        public Guid ReturnBillId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public Guid? WarehouseId { get; set; }
        public int? Status { get; set; }

        public virtual Warehou Warehou { get; set; }
        public virtual ICollection<ReturnBillDetail> ReturnBillDetails { get; set; }
    }
}
