﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class RecivedBill
    {
        public RecivedBill()
        {
            RecivedBillDetails = new HashSet<RecivedBillDetail>();
        }

        public Guid RecivedBillId { get; set; }
        public string Name { get; set; }
        public Guid? WarehouseId { get; set; }
        public Guid? EmployeeId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public int? Status { get; set; }

        public virtual User Employee { get; set; }
        public virtual Warehou Warehouse { get; set; }
        public virtual ICollection<RecivedBillDetail> RecivedBillDetails { get; set; }
    }
}
