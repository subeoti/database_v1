﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class WarehouseImportDetail
    {
        public Guid WarehouseImportBillDetailId { get; set; }
        public Guid WarehouseImportBillId { get; set; }
        public Guid ProductVariantId { get; set; }
        public int InportQuantity { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual WarehouseImportBill WarehouseImportBill { get; set; }
    }
}
