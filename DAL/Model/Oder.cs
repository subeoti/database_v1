﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Oder
    {
        public Oder()
        {
            PaymentDetails = new HashSet<PaymentDetail>();
        }

        public Guid OderItemId { get; set; }
        public Guid? CartSessionId { get; set; }
        public int? Quantity { get; set; }
        public decimal? Total { get; set; }
        public Guid? UserId { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual CartSession OderItem { get; set; }
        public virtual OderDetail OderDetail { get; set; }
        public virtual ICollection<PaymentDetail> PaymentDetails { get; set; }
    }
}
