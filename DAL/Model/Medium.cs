﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Medium
    {
        public Guid MediaId { get; set; }
        public Guid? ProductId { get; set; }
        public string? AbsolutePath { get; set; }
        public string? AbsoluteUri { get; set; }
        public string? ContentLength { get; set; }
        public string? FileName { get; set; }
        public string? Mime { get; set; }
        public string? OriginalFileName { get; set; }
        public string? RelativePath { get; set; }
        public bool? Status { get; set; }
    }
}
