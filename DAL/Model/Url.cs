﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Url
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Method { get; set; }
        public ICollection<RoleUrl> RoleUrls { get; set; }
    }
}
