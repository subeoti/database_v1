﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Warehou
    {
        public Warehou()
        {
            ReturnBills = new HashSet<ReturnBill>();
            DeliveryBillDetails = new HashSet<DeliveryBillDetail>();
            DeliveryBills = new HashSet<DeliveryBill>();
            RecivedBills = new HashSet<RecivedBill>();
            ReturnAcceptBills = new HashSet<ReturnAcceptBill>();
            WarehouseImportBills = new HashSet<WarehouseImportBill>();
        }

        public Guid WarehouseId { get; set; }
        public string WarehouseCode { get; set; }
        public string Name { get; set; }
        public Guid? ProductVariantId { get; set; }
        public int? Quantity { get; set; }
        public bool? Status { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual ICollection<ReturnBill> ReturnBills { get; set; }
        public virtual ICollection<DeliveryBillDetail> DeliveryBillDetails { get; set; }
        public virtual ICollection<DeliveryBill> DeliveryBills { get; set; }
        public virtual ICollection<RecivedBill> RecivedBills { get; set; }
        public virtual ICollection<ReturnAcceptBill> ReturnAcceptBills { get; set; }
        public virtual ICollection<WarehouseImportBill> WarehouseImportBills { get; set; }
    }
}
