﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ResetPasswordToken
    {
        public Guid Id { get; set; }

        public int Code { get; set; }

        public string Token { get; set; }

        public Guid UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ExpireTime { get; set; }
    }
}
