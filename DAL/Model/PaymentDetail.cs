﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class PaymentDetail
    {
        public Guid PaymentDetailId { get; set; }
        public Guid? OderItemId { get; set; }
        public decimal? Total { get; set; }
        public string Provider { get; set; }
        public string Status { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual Oder OderItem { get; set; }
    }
}
