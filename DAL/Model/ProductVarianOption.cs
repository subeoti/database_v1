﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class ProductVarianOption
    {
        public ProductVarianOption()
        {
            ProductVariantOptionValues = new HashSet<ProductVariantOptionValue>();
        }

        public Guid VarianOptionsId { get; set; }
        public Guid? ProductVarianId { get; set; }
        public string VarianOptionName { get; set; }
        public bool? Status { get; set; }

        public virtual ProductVariant ProductVarian { get; set; }
        public virtual ICollection<ProductVariantOptionValue> ProductVariantOptionValues { get; set; }
    }
}
