﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class RecivedBillDetail
    {
        public Guid RecivedBillDetailId { get; set; }
        public Guid? RecivedBillId { get; set; }
        public Guid? ProductVariantId { get; set; }
        public string ProductVariantIname { get; set; }
        public int? Quantity { get; set; }
        public int? NumberofRequest { get; set; }
        public int? ExportQuantity { get; set; }
        public int? Status { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual RecivedBill RecivedBill { get; set; }
    }
}
