﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class WarehouseImportBill
    {
        public WarehouseImportBill()
        {
            WarehouseImportDetails = new HashSet<WarehouseImportDetail>();
        }

        public Guid WarehouseImportBill1 { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public Guid WarehouseId { get; set; }
        public int Status { get; set; }

        public virtual Warehou Warehouse { get; set; }
        public virtual ICollection<WarehouseImportDetail> WarehouseImportDetails { get; set; }
    }
}
