﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DAL.Models
{
    public partial class DBContext : DbContext
    {
        public DBContext()
        {
        }

        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CartSession> CartSessions { get; set; }
        public virtual DbSet<CartSessionDetail> CartSessionDetails { get; set; }
        public virtual DbSet<DeliveryBill> DeliveryBills { get; set; }
        public virtual DbSet<DeliveryBillDetail> DeliveryBillDetails { get; set; }
        public virtual DbSet<Medium> Media { get; set; }
        public virtual DbSet<Oder> Oders { get; set; }
        public virtual DbSet<OderDetail> OderDetails { get; set; }
        public virtual DbSet<PaymentDetail> PaymentDetails { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductVarianOption> ProductVarianOptions { get; set; }
        public virtual DbSet<ProductVariant> ProductVariants { get; set; }
        public virtual DbSet<ProductVariantOptionValue> ProductVariantOptionValues { get; set; }
        public virtual DbSet<RecivedBill> RecivedBills { get; set; }
        public virtual DbSet<RecivedBillDetail> RecivedBillDetails { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserAddre> UserAddres { get; set; }
        public virtual DbSet<Voucher> Vouchers { get; set; }
        public virtual DbSet<Warehou> Warehous { get; set; }
        public virtual DbSet<Url> Urls { get; set; }
        public virtual DbSet<RoleUrl> RoleUrls { get; set; }
        public virtual DbSet<ResetPasswordToken> ResetPasswordTokens { get; set; }
        public virtual DbSet<WarehouseImportBill> WarehouseImportBills { get; set; }
        public virtual DbSet<WarehouseImportDetail> WarehouseImportDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-P8BV7UB;Initial Catalog=DuAnTotNghiep;Integrated Security=True");
            }
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {

        //        optionsBuilder.UseSqlServer("Data Source=DESKTOP-5BEQ4V0\\SQLEXPESS2012;Initial Catalog=DATN_PetWord;Integrated Security=True");
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<CartSession>(entity =>
            {
                entity.ToTable("cartSession");

                entity.Property(e => e.CartSessionId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Created_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.CartSessions)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__cartSessi__UserI__22751F6C");
            });

            modelBuilder.Entity<CartSessionDetail>(entity =>
            {
                entity.ToTable("cartSessionDetail");

                entity.Property(e => e.CartSessionDetailId).ValueGeneratedNever();

                entity.Property(e => e.CreateAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Create_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProductVariantId).HasColumnName("ProductVariant_Id");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.CartSessionDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .HasConstraintName("FK__cartSessi__Produ__2B0A656D");
            });

            modelBuilder.Entity<DeliveryBill>(entity =>
            {
                entity.HasKey(e => e.DeliveryId)
                    .HasName("PK__delivery__626D8FCEF5B74C8D");

                entity.ToTable("deliveryBill");

                entity.Property(e => e.DeliveryId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Created_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.DeliveryBills)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__deliveryB__Emplo__08B54D69");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.DeliveryBills)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK__deliveryB__Wareh__02FC7413");
            });

            modelBuilder.Entity<DeliveryBillDetail>(entity =>
            {
                entity.HasKey(e => e.DeliveryBillId)
                    .HasName("PK__delivery__A24F9EFA04CCF65F");

                entity.ToTable("deliveryBillDetails");

                entity.Property(e => e.DeliveryBillId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Created_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.ProductVariantId).HasColumnName("ProductVariant_Id");

                entity.HasOne(d => d.Delivery)
                    .WithMany(p => p.DeliveryBillDetails)
                    .HasForeignKey(d => d.DeliveryId)
                    .HasConstraintName("FK__deliveryB__Deliv__2EDAF651");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.DeliveryBillDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .HasConstraintName("FK__deliveryB__Produ__31B762FC");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.DeliveryBillDetails)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK__deliveryB__Wareh__02084FDA");
            });

            modelBuilder.Entity<Medium>(entity =>
            {
                entity.HasKey(e => e.MediaId)
                    .HasName("PK__media__B2C2B5CF0BFF1C7D");

                entity.ToTable("media");

                entity.Property(e => e.MediaId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Oder>(entity =>
            {
                entity.HasKey(e => e.OderItemId)
                    .HasName("PK__Oder__0B7EBA0F98D3700B");

                entity.ToTable("Oder");

                entity.Property(e => e.OderItemId).ValueGeneratedNever();

                entity.Property(e => e.CreateAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Create_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.OderItem)
                    .WithOne(p => p.Oder)
                    .HasForeignKey<Oder>(d => d.OderItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Oder__OderItemId__7E37BEF6");
            });

            modelBuilder.Entity<OderDetail>(entity =>
            {
                entity.ToTable("OderDetail");

                entity.Property(e => e.OderDetailId).ValueGeneratedNever();

                entity.Property(e => e.CreateAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Create_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.OderItemId).HasColumnName("OderItemID");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.OderDetailNavigation)
                    .WithOne(p => p.OderDetail)
                    .HasForeignKey<OderDetail>(d => d.OderDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OderDetai__OderD__75A278F5");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.OderDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .HasConstraintName("FK__OderDetai__Produ__29221CFB");
            });

            modelBuilder.Entity<PaymentDetail>(entity =>
            {
                entity.ToTable("paymentDetail");

                entity.Property(e => e.PaymentDetailId).ValueGeneratedNever();

                entity.Property(e => e.CreateAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Create_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Provider).HasMaxLength(255);

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .HasColumnName("status");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.OderItem)
                    .WithMany(p => p.PaymentDetails)
                    .HasForeignKey(d => d.OderItemId)
                    .HasConstraintName("FK__paymentDe__OderI__114A936A");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.Property(e => e.ProductId).ValueGeneratedNever();

                entity.Property(e => e.CreatAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Creat_At");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_At");

                entity.Property(e => e.ProductName).HasMaxLength(300);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK__products__Catego__19DFD96B");
            });

            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId)
                    .HasName("PK__productC__19093A2B7278C71F");

                entity.ToTable("productCategory");

                entity.Property(e => e.CategoryId)
                    .ValueGeneratedNever()
                    .HasColumnName("CategoryID");

                entity.Property(e => e.CategoryIcon)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryName).HasMaxLength(300);
            });

            modelBuilder.Entity<ProductVarianOption>(entity =>
            {
                entity.HasKey(e => e.VarianOptionsId)
                    .HasName("PK__productV__7B60FA54B67AE66D");

                entity.ToTable("productVarianOptions");

                entity.Property(e => e.VarianOptionsId).ValueGeneratedNever();

                entity.Property(e => e.VarianOptionName).HasMaxLength(100);

                entity.HasOne(d => d.ProductVarian)
                    .WithMany(p => p.ProductVarianOptions)
                    .HasForeignKey(d => d.ProductVarianId)
                    .HasConstraintName("FK__productVa__Produ__2A164134");
            });

            modelBuilder.Entity<ProductVariant>(entity =>
            {
                entity.ToTable("productVariant");

                entity.Property(e => e.ProductVariantId).ValueGeneratedNever();

                entity.Property(e => e.BarCode)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("Bar_code")
                    .IsFixedLength(true);

                entity.Property(e => e.CreateAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Create_At");

                entity.Property(e => e.InventoryQuantity).HasColumnName("Inventory_quantity");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_At");

                entity.Property(e => e.ProductVariantName).HasMaxLength(300);

                entity.Property(e => e.Sku).HasMaxLength(255);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductVariants)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_productVariant_products");
            });

            modelBuilder.Entity<ProductVariantOptionValue>(entity =>
            {
                entity.HasKey(e => e.VariantOptionValueId)
                    .HasName("PK__productV__6582C358F020A137");

                entity.ToTable("productVariantOptionValue");

                entity.Property(e => e.VariantOptionValueId)
                    .ValueGeneratedNever()
                    .HasColumnName("VariantOptionValueID");

                entity.Property(e => e.VariantOptionsValueName).HasMaxLength(100);

                entity.HasOne(d => d.VariantOptions)
                    .WithMany(p => p.ProductVariantOptionValues)
                    .HasForeignKey(d => d.VariantOptionsId)
                    .HasConstraintName("FK__productVa__Varia__17F790F9");
            });

            modelBuilder.Entity<RecivedBill>(entity =>
            {
                entity.ToTable("recivedBill");

                entity.Property(e => e.RecivedBillId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Created_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.RecivedBills)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__recivedBi__Emplo__09A971A2");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.RecivedBills)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK__recivedBi__Wareh__2FCF1A8A");
            });

            modelBuilder.Entity<RecivedBillDetail>(entity =>
            {
                entity.ToTable("RecivedBillDetail");

                entity.Property(e => e.RecivedBillDetailId).ValueGeneratedNever();

                entity.Property(e => e.ProductVariantIname)
                    .HasMaxLength(255)
                    .HasColumnName("ProductVariantIName");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.RecivedBillDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .HasConstraintName("FK__RecivedBi__Produ__367C1819");

                entity.HasOne(d => d.RecivedBill)
                    .WithMany(p => p.RecivedBillDetails)
                    .HasForeignKey(d => d.RecivedBillId)
                    .HasConstraintName("FK__RecivedBi__Reciv__5AEE82B9");
            });

            modelBuilder.Entity<ReturnAcceptBill>(entity =>
            {
                entity.ToTable("ReturnAcceptBill");

                entity.Property(e => e.ReturnAcceptBillId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ReturnAcceptBillName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.ReturnAcceptBills)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK_ReturnAcceptBill_warehous");
            });

            modelBuilder.Entity<ReturnAccpetBillDetail>(entity =>
            {
                entity.HasKey(e => e.ReturnAcceptBillDetailId);

                entity.ToTable("ReturnAccpetBillDetail");

                entity.Property(e => e.ReturnAcceptBillDetailId).ValueGeneratedNever();

                entity.Property(e => e.Note).HasMaxLength(250);

                entity.Property(e => e.ReturnAcceptBillId).HasColumnName("ReturnAcceptBillID");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.ReturnAccpetBillDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReturnAccpetBillDetail_productVariant");

                entity.HasOne(d => d.ReturnAcceptBill)
                    .WithMany(p => p.ReturnAccpetBillDetails)
                    .HasForeignKey(d => d.ReturnAcceptBillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReturnAccpetBillDetail_ReturnAcceptBill");
            });

            modelBuilder.Entity<ReturnBill>(entity =>
            {
                entity.ToTable("ReturnBill");

                entity.Property(e => e.ReturnBillId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Created_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.HasOne(d => d.Warehou)
                    .WithMany(p => p.ReturnBills)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK_ReturnBill_warehous");
            });

            modelBuilder.Entity<ReturnBillDetail>(entity =>
            {
                entity.ToTable("ReturnBillDetail");

                entity.Property(e => e.ReturnBillDetailId).ValueGeneratedNever();

                entity.HasOne(d => d.ReturnBillDetailNavigation)
                    .WithMany(p => p.ReturnBillDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .HasConstraintName("FK_ReturnBillDetail_productVariant");

                entity.HasOne(d => d.ReturnBill)
                    .WithMany(p => p.ReturnBillDetails)
                    .HasForeignKey(d => d.ReturnBillId)
                    .HasConstraintName("FK_ReturnBillDetail_ReturnBill");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.RoleId).ValueGeneratedNever();

                entity.Property(e => e.RoleName).HasMaxLength(255);

                entity.HasData(
                    new Role
                    {
                        RoleId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                        RoleName = "Admin",
                        IsActive = true
                    });
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasData(
                   new User
                   {
                       FirsName = "Admin",
                       LastName = "Admin",
                       CreateAt = DateTime.Now,
                       PassWord = "Test@123",
                       PhoneNumber = "0123456789",
                       RoleId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                       Email = "admin@test.tt",
                       UserId = Guid.Parse("921dad0f-65cd-4dca-b274-ca198b556409"),
                       UserName = "administrator",
                   });

                entity.ToTable("user");

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever()
                    .HasColumnName("UserID");

                entity.Property(e => e.Avatar).HasMaxLength(255);

                entity.Property(e => e.CreateAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Create_at");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirsName).HasMaxLength(255);

                entity.Property(e => e.LastName).HasMaxLength(255);

                entity.Property(e => e.ModifieAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modifie_at");

                entity.Property(e => e.PassWord).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(255);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserName).HasMaxLength(255);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__user__RoleID__0C85DE4D");
            });

            modelBuilder.Entity<UserAddre>(entity =>
            {
                entity.HasKey(e => e.UserAddressId)
                    .HasName("PK__user_add__5961BB97BA5D2B0C");

                entity.ToTable("user_addres");

                entity.Property(e => e.UserAddressId)
                    .ValueGeneratedNever()
                    .HasColumnName("UserAddressID");

                entity.Property(e => e.AddressDetail).HasMaxLength(255);

                entity.Property(e => e.CityDistrict)
                    .HasMaxLength(255)
                    .HasColumnName("City_District");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(255);

                entity.Property(e => e.Province).HasMaxLength(255);

                entity.Property(e => e.TypeAdress).HasMaxLength(255);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.WardCommune)
                    .HasMaxLength(255)
                    .HasColumnName("Ward_Commune");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAddres)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__user_addr__UserI__0D7A0286");
            });

            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.Property(e => e.VoucherId).ValueGeneratedNever();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Warehou>(entity =>
            {
                entity.HasKey(e => e.WarehouseId)
                    .HasName("PK__warehous__2608AFF9B58680D5");

                entity.ToTable("warehous");

                entity.Property(e => e.WarehouseId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Created_at");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Modified_at");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.ProductVariantId).HasColumnName("ProductVariant_Id");

                entity.Property(e => e.WarehouseCode).HasMaxLength(255);

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.Warehous)
                    .HasForeignKey(d => d.ProductVariantId)
                    .HasConstraintName("FK__warehous__Produc__2BFE89A6");
            });

            modelBuilder.Entity<WarehouseImportBill>(entity =>
            {
                entity.HasKey(e => e.WarehouseImportBill1);

                entity.ToTable("WarehouseImportBill");

                entity.Property(e => e.WarehouseImportBill1)
                    .ValueGeneratedNever()
                    .HasColumnName("WarehouseImportBill");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.WarehouseImportBills)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WarehouseImportBill_warehous");
            });

            modelBuilder.Entity<WarehouseImportDetail>(entity =>
            {
                entity.HasKey(e => e.WarehouseImportBillDetailId);

                entity.ToTable("WarehouseImportDetail");

                entity.Property(e => e.WarehouseImportBillDetailId).ValueGeneratedNever();

                entity.Property(e => e.Note).HasMaxLength(250);

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.WarehouseImportDetails)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WarehouseImportDetail_productVariant");

                entity.HasOne(d => d.WarehouseImportBill)
                    .WithMany(p => p.WarehouseImportDetails)
                    .HasForeignKey(d => d.WarehouseImportBillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WarehouseImportDetail_WarehouseImportBill");
            });

            AuthorizeModelConfig(modelBuilder);
            OnModelCreatingPartial(modelBuilder);
            ResetPasswordModelConfig(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public static void AuthorizeModelConfig(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Url>(e =>
            {
                e.HasIndex(url => new { url.Path, url.Method }).IsUnique();
                e.HasKey(url => url.Id);
                e.Property(url => url.Path).IsRequired();
            });
            modelBuilder.Entity<RoleUrl>(e =>
            {
                e.HasKey(rl => rl.Id);
                e.HasIndex(rl => new { rl.RoleId, rl.UrlId }).IsUnique();
                e.HasOne(rl => rl.Role)
                .WithMany(r => r.RoleUrls)
                .HasForeignKey(r => r.RoleId);

                e.HasOne(rl => rl.Url)
                .WithMany(u => u.RoleUrls)
                .HasForeignKey(r => r.UrlId);
            });
        }

        public static void ResetPasswordModelConfig(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ResetPasswordToken>(entity =>
            {
                entity.HasKey(k => k.Id);
                entity.Property(p => p.UserId).IsRequired();
                entity.Property(p => p.Code).IsRequired().HasMaxLength(5);
            });
        }

       
    }
}
